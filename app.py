from flask import Flask, render_template
from time import sleep

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.firefox.options import Options

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/page2')
def page2():
    return render_template('page2.html')

@app.route('/bot')
def bot():
    driver = webdriver.Remote(
        command_executor='http://localhost:4444/wd/hub',
        options=webdriver.FirefoxOptions()
    )

    driver.get('https://www.google.ca')
    sleep(5)
    search_box = driver.find_element('id', 'APjFqb')
    search_box.send_keys('My Selenium Search')
    sleep(5)
    search_box.send_keys(Keys.RETURN)

    return 'Bot search activated'