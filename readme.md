# Description
Démo pour l'utilisation de Selenium avec Python

# Instructions de démarrage

## Instructions pour Selenium
Démarrer le docker `selenium` avec la commande `docker compose up`

## Instructions pour Flask
Installer les requis et lancer l'application
```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
flask run
```

# Liens

## Selenium
Selenium : http://localhost:4444
*Note: Le mot de passe pour les sessions VNC est `secret`*

## Flask
Flask : http://localhost:5000

# Démo
![demo.gif](demo.gif)